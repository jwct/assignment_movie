# Assignment 
This is the assignment of Web Development with Java Spring Framework in Coursera.

The task is to develop of RESTFull Web Services with Spring MVC and Hibernate CRUD on a movie library

# Configuration
1. Edit pom.xml , find the following section. Then replace the correct jdbc connection url , database username and database password.
```             
            <plugin>
                <groupId>org.flywaydb</groupId>
                <artifactId>flyway-maven-plugin</artifactId>
                <version>4.0.3</version>
                <configuration>
		    <driver>org.postgresql.Driver</driver>
		    <url>jdbc:postgresql://pg_server:5432/movie</url>
                    <user>postgres</user>
                    <password>1234</password>
		    <baselineOnMigrate>true</baselineOnMigrate>
		    <baselineVersion>1</baselineVersion>
                </configuration>
            </plugin> 
```
2. Edit src/main/resources/application.properties , find the following section. Then replace the correct jdbc connection url , database username and database password.
```
spring.datasource.url=jdbc:postgresql://pg_server:5432/movie
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.username=postgres
spring.datasource.password=1234
```

# Build and run 
- In the project root directory, issue the following command. Maven will re-build and start the application
```
./mvnw spring-boot:run 
```
or 
```
./run.sh
```

# Test
0. Assuming that curl is installed. For details, please refer to https://curl.se.

1. List all movies from database
Command :
```
curl -X GET "http://localhost:8080/movie/movie"
```
Expected output : 
```
{"content":[{"id":1,"name":"Tron (1982)","userRating":6.80},{"id":2,"name":"Star Wars: Episode IV - A New Hope (1977)","userRating":8.60}],"pageable":{"sort":{"sorted":true,"unsorted":false,"empty":false},"pageNumber":0,"pageSize":10,"offset":0,"unpaged":false,"paged":true},"totalElements":2,"last":true,"totalPages":1,"sort":{"sorted":true,"unsorted":false,"empty":false},"numberOfElements":2,"first":true,"size":10,"number":0,"empty":false}
```

2. Create a new movie
Command :
```
curl -X POST "http://localhost:8080/movie/movie" -H "Content-Type:application/json" -H "Accept:application/json" -d '{"name" : "Star Wars: Episode I - The Phantom Menace (1999)" , "userRating" : "6.5"}'
```
Expected output :
```
{"id":3,"name":"Star Wars: Episode I - The Phantom Menace (1999)","userRating":6.5}
```

3. Update existing record.
Command:
```
curl -X PUT "http://localhost:8080/movie/movie/1" -H "Content-Type:application/json" -H "Accept:application/json" -d '{"name" : "Star Wars: Episode VII - The Force Awakens (2015)" , "userRating" : "7.9"}'
```
Expected output:
```
{"id":1,"name":"Star Wars: Episode VII - The Force Awakens (2015)","userRating":7.9}
```

4. Get single record
Command :
```
curl -X GET "http://localhost:8080/movie/movie/1"
```
Expected output
```
{"id":1,"name":"Star Wars: Episode VII - The Force Awakens (2015)"
```
5. Delete a record
Command:
```
curl -X DELETE "http://localhost:8080/movie/movie/3"
```
Expected no output should be result.

# Remarks
- This assignment is built and tested in the following environment 
1. OpenJDK 8 docker
2. PostgreSQL 13.3 docker
3. curl 7.64.0 (bundled in OpenJDK 8 docker)
