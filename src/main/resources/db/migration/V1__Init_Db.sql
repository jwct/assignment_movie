CREATE SEQUENCE fc_movie_id_seq
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  MAXVALUE 2147483647
  CACHE 1;

CREATE TABLE fc_movie (
  id int8 NOT NULL DEFAULT nextval('fc_movie_id_seq'),
  name VARCHAR(255),
  user_rating NUMERIC(5,2),
  PRIMARY KEY(id));
