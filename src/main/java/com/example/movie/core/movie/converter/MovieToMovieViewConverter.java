package com.example.movie.core.movie.converter;

import com.example.movie.core.movie.Movie;
import com.example.movie.core.movie.web.MovieView;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class MovieToMovieViewConverter implements Converter<Movie, MovieView> {

    @Override
    public MovieView convert(@NonNull Movie movie) {
        MovieView view = new MovieView();
        view.setId(movie.getId());
        view.setName(movie.getName());
        view.setUserRating(movie.getUserRating());
        return view;
    }
}
