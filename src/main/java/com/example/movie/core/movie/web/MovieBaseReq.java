package com.example.movie.core.movie.web;

import com.example.movie.base.BaseRequest;
import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class MovieBaseReq extends BaseRequest {

    @NotEmpty
    private String name;

    @NotNull
    private BigDecimal userRating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUserRating() {
        return userRating;
    }

    public void setUserRating(BigDecimal userRating) {
        this.userRating = userRating;
    }
}
