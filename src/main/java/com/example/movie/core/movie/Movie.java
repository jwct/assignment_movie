package com.example.movie.core.movie;

import org.hibernate.annotations.GenericGenerator;
import java.math.BigDecimal;
import javax.persistence.*;

@Entity
@Table(name = "fc_movie")
public class Movie {

    @Id
    @Column(name = "id")

    @GenericGenerator(
       name = "fc_movie_id_seq",
       strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
       parameters =
       {
	   @org.hibernate.annotations.Parameter(name = "sequence_name", value = "fc_movie_id_seq"),
	   @org.hibernate.annotations.Parameter(name= "INCREMENT", value = "1"),
	   @org.hibernate.annotations.Parameter(name = "MINVALUE", value = "1"),
	   @org.hibernate.annotations.Parameter(name = "MAXVALUE", value = "2147483647"),
	   @org.hibernate.annotations.Parameter(name = "CACHE", value = "1")
	   
       }
    )

    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fc_movie_id_seq")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "user_rating" )
    private BigDecimal userRating;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUserRating() {
        return userRating;
    }

    public void setUserRating(BigDecimal userRating) {
        this.userRating = userRating;
    }

}
