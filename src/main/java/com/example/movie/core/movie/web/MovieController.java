package com.example.movie.core.movie.web;

import com.example.movie.core.movie.Movie;
import com.example.movie.core.movie.MovieService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/movie")
public class MovieController {

    private final MovieService service;

    public MovieController(MovieService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public MovieView getMovie(@PathVariable Long id) {
        return service.getMovie(id);
    }

    @GetMapping
    @ResponseBody
    public Page<MovieView> getAllMovie(@PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
        return service.findAllMovie(pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public MovieView create(@RequestBody @Valid MovieBaseReq req) {
        return service.create(req);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMovie(@PathVariable Long id){
        service.delete(id);
    }

    @PutMapping("/{id}")
    public MovieView updateMovie(@PathVariable(name = "id") Long id,
                              @RequestBody @Valid MovieBaseReq req){
        Movie movie = service.findMovieOrThrow(id);
        return service.update(movie, req);
    }
}

