package com.example.movie.core.movie.web;

import java.math.BigDecimal;

public class MovieView {

    private Long id;
    private String name;
    private BigDecimal userRating;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public BigDecimal getUserRating() {
        return userRating;
    }

    public void setUserRating(BigDecimal userRating) {
        this.userRating = userRating;
    }

}
