package com.example.movie.core.movie;


import com.example.movie.core.movie.converter.MovieToMovieViewConverter;
import com.example.movie.core.movie.web.MovieBaseReq;
import com.example.movie.core.movie.web.MovieView;
import com.example.movie.error.EntityNotFoundException;
import com.example.movie.util.MessageUtil;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    private final MovieRepo movieRepo;
    private final MovieToMovieViewConverter movieToMovieViewConverter;
    private final MessageUtil messageUtil;

    public MovieService(MovieRepo movieRepo,
                       MovieToMovieViewConverter movieToMovieViewConverter,
                       MessageUtil messageUtil) {
        this.movieRepo = movieRepo;
        this.movieToMovieViewConverter = movieToMovieViewConverter;
        this.messageUtil = messageUtil;
    }

    public MovieView getMovie(Long id) {
        Movie movie = findMovieOrThrow(id);
        return movieToMovieViewConverter.convert(movie);
    }

    public Movie findMovieOrThrow(Long id) {
        return movieRepo.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(messageUtil.getMessage("movie.NotFound", id)));
    }

    public Page<MovieView> findAllMovie(Pageable pageable){
        Page<Movie> movies = movieRepo.findAll(pageable);
        List<MovieView> movieViews = new ArrayList<>();
        movies.forEach(movie -> {
            MovieView movieView = movieToMovieViewConverter.convert(movie);
            movieViews.add(movieView);
        });
        return new PageImpl<>(movieViews, pageable, movies.getTotalElements());
    }

    public MovieView create(MovieBaseReq req) {
        Movie movie = new Movie();
        this.prepare(movie,req);
        Movie movieSave = movieRepo.save(movie);
        return movieToMovieViewConverter.convert(movieSave);
    }

    @Transactional
    public void delete(Long id) {
        try {
            movieRepo.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(messageUtil.getMessage("movie.NotFound", id));
        }
    }

    public MovieView update(Movie movie, MovieBaseReq req){
        Movie newMovie = this.prepare(movie,req);
        Movie tournamentSave = movieRepo.save(newMovie);
        return movieToMovieViewConverter.convert(tournamentSave);
    }

    public Movie prepare(Movie movie, MovieBaseReq movieBaseReq){
        movie.setName(movieBaseReq.getName());
        movie.setUserRating(movieBaseReq.getUserRating());
        return movie;
    }
}
